<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Delete_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function berita($id){
        $this->db->delete('pol_berita',array('id'=>$id));
    }
    function ponline($param){
        $this->db->where('id',$param['id']);
        return $this->db->update('pol_ponline',array('is_delete' => '1', 'reason_delete' => $param['alasan']));
    }
    function psms($param){
        $this->db->where('id',$param['id']);
        return $this->db->update('pol_psms',array('is_delete' => '1', 'reason_delete' => $param['alasan']));
    }
    function faq($param){
         $this->db->delete('pol_faq', array('id' => $param['id'])); 
    }
    function faq_kategori($param){
         $this->db->delete('pol_faq_kategori', array('id' => $param['id'])); 
    }
}
?>