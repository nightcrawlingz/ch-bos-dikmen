<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Update_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function faq($id,$data){
        $this->db->where('id',$id);
        return $this->db->update('pol_faq',$data);
    }
    function faq_kategori($id,$data){
        $this->db->where('id',$id);
        return $this->db->update('pol_faq_kategori',$data);
    }
    function berita($id,$data){
        $this->db->where('id',$id);
        return $this->db->update('pol_berita',$data);
    }
    
    function user($id,$data){
        $this->db->where('id',$id);
        return $this->db->update('pol_user',$data);
    }
    function ponline($id,$data){
        $this->db->where('id',$id);
        return $this->db->update('pol_ponline',$data);
    }
    
    function t_user($id, $data){
        $this->db->where('id',$id);
        return $this->db->update('t_user',$data);
    }
    function mo_sms($id, $data){
        return $this->db->like();
    }
    function psms($id, $data){
        $this->db->where('id',$id);
        return $this->db->update('pol_psms',$data);
    }
    function psms1($id, $data){
        if(isset($id['id']))
            $this->db->where('id',$id['id']);
        if(isset($id['id_sms']))
            $this->db->where('id_sms',$id['id_sms']);
        
        return $this->db->update('pol_psms',$data);
    }
    function send_email($id, $data){
      $this->db->where('id',$id);
      return $this->db->update('pol_send_email',$data);
    }
    
    function pengaduan($id, $data){
        $this->db->where('id',$id);
        $this->db->update('g3n_pengaduan',$data);
    }
    
    function respon($id,$data){
        $this->db->where('id',$id);
        $this->db->update('g3n_update',$data);
    }
    
    function user_handling($id,$data_user,$role){
        $this->db->trans_start();
        $this->db->where('id',$id);
        if($this->db->update('t_user',$data_user)){
            
            $this->db->delete('pol_role_jenjang',array('user_id' => $id));
            
            foreach($role as $role_id){
                $this->db->insert('pol_role_jenjang', array('user_id' => $id, 'jenjang_id' => $role_id));
            }
        }
        
        $this->db->trans_complete();
    }
}
?>