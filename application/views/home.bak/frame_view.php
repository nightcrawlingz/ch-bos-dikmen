<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head.php"); ?>
<body>
<style type="text/css">
        iframe {
			position:absolute;
            top: 0; left: 0; width: 100%; height: 100%;
            border: none; padding-top: 100px;
            box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box;
        }
    </style>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <h1><a href="#"><span>Layanan Masyarakat & </span>Penanganan Pengaduan</a> <small><!--Simple design templates--></small></h1>
      </div>
      <div class="clr"></div>
	  <?php require_once("inc/menu.php") ?>
    </div>
  </div>
  <div class="content">
    <div class="content_resize">
		<iframe src="http://bos.kemdiknas.go.id/pengaduan/aplikasi"></iframe>
      <div class="clr"></div>
    </div>
  </div>
  <?php require_once("inc/footer.php") ?>
  </div>
</div>
</body>
</html>
