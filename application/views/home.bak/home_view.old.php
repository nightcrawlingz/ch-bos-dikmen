<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head.php"); ?>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <?php require_once('inc/logo.php') ?>
      <div class="clr"></div>
	  <?php require_once("inc/menu.php") ?>
    </div>
  </div>
  <div class="content">
    <div class="content_resize"> 
      <div class="clr"></div>
	  <div class="sidebar1">
        <?php require_once('inc/form_chart.php') ?>
      </div>
	  <div class="mainbar">
        <div class="article">
          
          <div class="clr"></div>
          <p class="infopost"><!--Posted <span class="date">on 11 sep 2018</span> by <a href="#">Owner</a> &nbsp;|&nbsp; Filed under <a href="#">templates</a>, <a href="#">internet</a> with <a href="#" class="com">Comments <span>11</span></a>--></p>
		  <div align="center">
		  <div id="piecemaker">
      <p>Put your alternative Non Flash content here.</p>
    </div>
	</div>
	<div class="sidebar" style="width:250px">
	 
        <div class="gadget" >
          <h2 class="star"><span><img src="<?=$this->config->item('home_img')?>/artikel.png">Artikel</span></h2>
          <div class="clr"></div>
          <ul class="ex_menu">
		  <?php foreach($berita->result() as $row):?>
            <li><a href="<?=site_url('home/berita/view/'.$row->id)?>"><?=$row->judul?></a><br /><span style="color:#000000;">
              <?=$row->ringkasan?></span></li>
          <?php endforeach;?>
		  	<li><a href="<?=site_url('home/berita')?>">Index</a></li>
          </ul>
        </div>
      </div>
	  <div class="sidebar" style="width:250px; float:left">
	 
        <div class="gadget" >
          <h2 class="star"><span><img src="<?=$this->config->item('home_img')?>/berita.png">Berita</span></h2>
          <div class="clr"></div>
          <ul class="ex_menu">
		  <?php foreach($berita->result() as $row):?>
            <li><a href="<?=site_url('home/berita/view/'.$row->id)?>"><?=$row->judul?></a><br /><span style="color:#000000;">
              <?=$row->ringkasan?></span></li>
          <?php endforeach;?>
		  	<li><a href="<?=site_url('home/berita')?>">Index</a></li>
          </ul>
        </div>
      </div>
			<div style="margin-top:4px; float:left">
			<h2><span>Pengaduan Berdasarkan Kategori </span>&amp; Status Penanganan </h2>
			<div id="chart" class="FusionCharts-container" align="center"><!--Chart Di Load disini--></div>
			<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>
			<style>
			div.chart_table table{
				border-collapse:collapse;
			}
			div.chart_table table, td, th{
				border:1px solid black;
				color:#000000;
			}
			
			</style>
			<div align="center" class="chart_table"><?=$graph_table?></div>
            <p>&nbsp;</p>
			</div>
            <p>&nbsp;</p>
        </div>
        <p class="pages"><!--<small>Page 1 of 2 &nbsp;&nbsp;&nbsp;</small> <span>1</span> <a href="#">2</a> <a href="#">&raquo;</a>--></p>
      </div>
      <div class="sidebar">
	 
        <div class="gadget">
          <h2 class="star"><a href="#"><span><img src="<?=$this->config->item('home_img')?>/download.png">Unduh</span></a></h2>
          <div class="clr"></div>
          
        </div>
		
		<div class="gadget">
          <h2 class="star"><span><img src="<?=$this->config->item('home_img')?>/stat.png">Statistik Pengunjung</span></h2>
          <div class="clr"></div>
          <?php 
					function link_stat($param){
						return 'http://bos.kemdiknas.go.id/analytics/index.php?module=Widgetize&action=iframe&columns[]='.$param.'&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=1&period=range&date=last30&disableLink=1&widget=1&language=id';
						//http://bos.kemdiknas.go.id/analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitsSummary&actionToWidgetize=index&idSite=1&period=range&date=last30&disableLink=1&widget=1&language=id
					}
					function link_springkle($param){
						return 'http://bos.kemdiknas.go.id/analytics/index.php?module=VisitsSummary&action=getEvolutionGraph&idSite=1&period=range&date=last30&language=id&viewDataTable=sparkline&columns='.$param;
					}
					$summary=$this->initlib->summary_stat();
					function link_country(){
						return 'http://bos.kemdiknas.go.id/analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=GeoIP&actionToWidgetize=getGeoIPCountry&idSite=1&period=range&date=last30&disableLink=1&widget=1&language=id';
					}
					?>
					<ul style="list-style:none">
						<li>
						<div class="sparkline" style="border-bottom: 1px solid white;">
<a class="iframe" href="<?=link_stat('nb_visits')?>"><img width="100" height="25" src="<?=link_springkle('nb_visits')?>" alt="" class="sparkline"></a> <br />
<strong><?=number_format($summary['nb_visits'], 0, ',', '.')?></strong> kunjungan	</div>
						</li>
						<li>
						<div class="sparkline" style="border-bottom: 1px solid white;">
<a class="iframe" href="<?=link_stat('avg_time_on_site')?>"><img width="100" height="25" src="<?=link_springkle('avg_time_on_site')?>" alt="" class="sparkline"></a> <br />
<strong><?=intval(date("i", $summary['avg_time_on_site'])); ?> menit <?=intval(date("s", $summary['avg_time_on_site'])); ?>s</strong> rata-rata waktu kunjungan</div>
						</li>
						<li>
						<div class="sparkline" style="border-bottom: 1px solid white;">
<a class="iframe" href="<?=link_stat('bounce_rate')?>"><img width="100" height="25" src="<?=link_springkle('bounce_rate')?>" alt="" class="sparkline"></a> <br />
<strong><?=$summary['bounce_rate']?></strong> kunjungan dengan pentalan (meninggalkan situs setelah mengunjungi satu halaman)</div>
						</li>
						<li>
						<div class="sparkline" style="border-bottom: 1px solid white;">
<a class="iframe" href="<?=link_stat('nb_actions_per_visit')?>"> <img width="100" height="25" src="<?=link_springkle('nb_actions_per_visit')?>" alt="" class="sparkline"></a> <br />
<strong><?=$summary['nb_actions_per_visit']?></strong> tindakan tiap kunjungan</div>
						</li>
						
						<li>
						<div class="sparkline" style="cursor: pointer; border-bottom: 1px solid white;" >
						<a href="<?=link_country()?>" class="iframe" style="color:#000000">
						<style>
						.border_none td, .border_none th{
	border:none;
	color:#000000;
}
						</style>
						<table class="border_none">

						

						<?php foreach($this->initlib->stat_country() as $stat):?>

						<tr>

							<td><img src='http://bos.kemdiknas.go.id/analytics/<?=$stat['logo']?>' ></td>

							<td><?=$stat['label']?></td>

							<td><?=number_format($stat['nb_visits'], 0, ',', '.')?></td>
						</tr>

						<?php endforeach;?>
					</table>
					</a>
</div>
						</li>
					</ul>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <?php require_once("inc/footer.php") ?>
</div>
</div>
</body>
</html>
