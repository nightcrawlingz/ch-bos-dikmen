<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head.php"); ?>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <?php require_once('inc/logo.php') ?>
      <div class="clr"></div>
      <?php require_once("inc/menu.php"); ?>
    </div>
  </div>
  <div class="content">
    <div class="content_resize"> 
      <div class="clr"></div>
	  <div class="mainbar" style="width:inherit">
        <div class="article">
          <h2><img src="<?=$this->config->item('home_img')?>/<?=$this->uri->segment(2)=='faq' ? 'faq.png' : 'form.png'?>" width="48" style="border:0; padding:0;"/><a href="<?=$this->uri->segment(3)?>"><?=$title?></a></h2>
		  <style>
		  		#message-blue	{
				margin-bottom: 5px;
				}
			.blue-left	{
				background: url(<?=$this->config->item('admin_img')?>/table/message_blue.gif) top left no-repeat;
				color: #2e74b2;
				font-family: Tahoma;
				font-weight: bold;
				padding: 0 0 0 20px;
				}
			.blue-left a	{
				color: #2e74b2;
				font-family: Tahoma;
				font-weight: normal;
				text-decoration: underline;
				}
			.blue-right a	{
				cursor: pointer;
				}
			.blue-right	{
				width: 55px;}
		  </style>
		  <script>
			  $(document).ready(function() {
					$(".close-blue").click(function () {
						$("#message-blue").fadeOut("slow");
					});
					
				});
		  </script>
		  
          <div class="clr"></div>
          <p class="infopost"><!--Posted <span class="date">on 11 sep 2018</span> by <a href="#">Owner</a> &nbsp;|&nbsp; Filed under <a href="#">templates</a>, <a href="#">internet</a> with <a href="#" class="com">Comments <span>11</span></a>--></p>
			<?php if($this->uri->segment(2)=="pengaduan"):?>
			<div id="message-blue">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="blue-left">Mengadu secara komplit dan bertanggung jawab
sebelum anda bertanya lebih jauh, silahkan <a href="<?=site_url('home/faq');?>">lihat FAQ</a></td>
					<td class="blue-right"><a class="close-blue"><img src="<?=$this->config->item('admin_img')?>/table/icon_close_blue.gif"   alt="" style="border:0; margin:0;padding:0" /></a></td>
				</tr>
				</table>
		  </div>
			<form action="<?=site_url('home/post_pengaduan')?>" method="post" class="uniForm">
			<?php if($this->session->flashdata('valid')=='true'):?>
			<div id="okMsg">
			<p>
			  Terima kasih, telah berkontribusi untuk meningkatkan kualitas pendidikan yang lebih baik</p>
     		</div>
			<?php endif;?>
		<fieldset class="inlineLabels">
			<h4>Lokasi Kejadian</h4>
			<div class="ctrlHolder">
			  <label for="">Daerah</label><ul class="alternate">
			  	<li>
					
					Provinsi
					<select name="KdProv" id="provinsi" class="selectInput required">
                      <option value="">-Pilih-</option>
                      <?php foreach($prov->result() as $row):?>
                      <option value="<?=$row->KdProv?>">
                        <?=$row->NmProv?>
                      </option>
                      <?php endforeach;?>
                    </select>

				</li>
				<li>
					
					Kabupaten / Kota
					<select id="kabkota" name="KdKab" class="selectInput required" >
                      <option value="">-Pilih-</option>
                    </select>
					
				</li>
				<li>
					
					Kecamatan
					<select id="kecamatan" name="KdKec" class="selectInput">
                      <option value="">-Pilih-</option>
                    </select>
					
				</li>
			  </ul>
			  
			  <p class="formHint">&nbsp;</p>
        	</div>
			
			<div class="ctrlHolder">
			  <label for="">Jenjang</label>
			  <select name="KdJenjang" id="jenjang" class="medium required">
						<option value="">-Pilih-</option>
						<option value="1">SD</option>
						<option value="2">SMP</option>
						<option value="3">SD & SMP</option>
						<option value="4">Lainnya</option>
			  </select>
			  <p class="formHint">&nbsp;</p>
        	</div>
			
			<div class="ctrlHolder">
			
			  <label for="" id="namasek">Nama Sekolah </label>
			  <input type="text" name="NmSekolah" class="textInput medium required"/>
			  <p class="formHint">&nbsp;</p>
        	</div>	
			</fieldset>
			<fieldset class="inlineLabels">
			<h4>Kategori Pengaduan</h4>
			<div class="ctrlHolder">
			  
				<label for="">Kategori</label>
				<select name="KdKategori" id="KdKategori" class="medium required KdKategori">
                  <option value="">-Pilih-</option>
                  <?php foreach($kategori->result() as $row):?>
				  	<option value="<?=$row->KdKat?>"><?=$row->short?></option>
				  <?php endforeach;?>
                </select>
				<p class="formHint">&nbsp;</p>
			  <p class="formHint">&nbsp;</p>
        	</div>	
			<div class="ctrlHolder" id="KetRpKat4" style="display:none">
			  <label for="">Indikasi Dana yang Disimpangkan (Rp)</label>
			  	<input type="text" name="KetRpKat4" class="textInput auto validateInteger" id="KetRpKat4_text"/>
			  <p class="formHint">&nbsp;</p>
        	</div>
			</fieldset>
		<fieldset class="inlineLabels">
			<h4>Identitas Pelapor </h4>
			<div class="ctrlHolder" >
			  <label for="">Profesi</label>
			  	<select class="medium required" name="KdSumber">
						<option value="">-Pilih-</option>
						<?php //print_r($sumber_info->result())?>
						<?php foreach($sumber_info->result() as $row):?>
						<option value="<?=$row->KdSumber?>"><?=$row->Keterangan?></option>
						<?php endforeach;?>
			  </select>
			  
        	</div>
			<div class="ctrlHolder">
			  <label>Nama</label>
			  <ul>
			  
			  <li>
			  	<label><p style="display:none">Nama</p>
			  	<input type="text" class="textInput medium required" name="nama"/><br />
			  
			  	</label>
			  </li>
			  <li>
			  <label>Apakah anda ingin nama anda ditampilkan? <input type="checkbox" name="tampil_nama" value="1" />
			  </label>
			  </li>
			  </ul>
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			  <label for="">Email</label>
			  	<input type="text" class="textInput medium validateEmail required" name="email"/>
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			  	<label>Telp</label>
				<ul>
				<li>
				<label for=""><p style="display:none">Telp</p>
			  	<input type="text" class="textInput medium" name="telp"/>
				</label>
				
				</li>
				<li>
				  <label>Apakah anda ingin telp anda ditampilkan? <input type="checkbox" name="tampil_telp" value="1"/></label>
				  </li>
				</ul>
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			<label>Alamat</label>
			<ul>
			<li>
			  <label for=""><p style="display:none">Alamat</p>
			  	<textarea class="medium required" name="alamat" style="height:5em"></textarea>
			  </label>
			</li>
			<li>
				  <label>Apakah anda ingin alamat anda ditampilkan? 
				  <input type="checkbox" name="tampil_alamat" value="1"/></label>
			    </li>
			</ul>
			  <p class="formHint">&nbsp;</p>
        	</div>
				
			</fieldset>
			<fieldset class="inlineLabels">
			<h4>Deskripsi</h4>
			<div class="ctrlHolder">
			  <label for=""></label>
			  	<textarea name="Deskripsi" class="medium required " ></textarea>
			  <p class="formHint">&nbsp;</p>
        	</div>
			
			</fieldset>
			
			<div class="buttonHolder">
        	
        	<button class="primaryAction" type="submit">Kirim</button>
      </div>
</form>
<script type="text/javascript" src="<?=$this->config->item('home_js')?>/uni-form.jquery.js"></script>
	 <script type="text/javascript" src="<?=$this->config->item('home_js')?>/uni-form-validation.jquery.js"></script>
	 <script type="text/javascript">
       $(function(){
        $('form.uniForm').uniform({
			prevent_submit          : true
        });
      });
       </script>
			<?php elseif($this->uri->segment(2)=="faq"):?>
			<div id="message-blue">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="blue-left">Klik pertanyaan untuk menampilkan jawaban</td>
					<td class="blue-right"><a class="close-blue"><img src="<?=$this->config->item('admin_img')?>/table/icon_close_blue.gif"   alt="" style="border:0; margin:0;padding:0" /></a></td>
				</tr>
				</table>
		  </div>
				<?=$faq->isi?>
			<?php endif;?>
          <p>&nbsp;</p>
        </div>
        <p class="pages"><!--<small>Page 1 of 2 &nbsp;&nbsp;&nbsp;</small> <span>1</span> <a href="#">2</a> <a href="#">&raquo;</a>--></p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <?php require_once("inc/footer.php") ?>
</div>
</div>
</body>
</html>
