    <?php
	$halaman = $this->uri->segment(2);
    ?>
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
		    <?php
			$list_halaman = array(
			    'berita' => 'Berita',
			    'pengumuman' => 'Pengumuman',
			    'peraturan' => 'Peraturan Perundangan',
			    'publikasi' => 'Publikasi'
			);
		    ?>
                    <h6><a href="<?=site_url('')?>">Home</a></h6>
                    <h6><a href="<?=site_url('halaman')?>">Halaman</a></h6>
                    <h6><span class="page-active"><?=$list_halaman[$halaman]?></span></h6>
                    
                    
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-8">
                <div class="row">

                    <div class="col-md-12">
                        
                        <?php if($halaman_main->num_rows()) foreach($halaman_main->result() as $row):?>
                        <?php  if($row->published):?>
			    <div class="list-event-item">
				<div class="box-content-inner clearfix">
				    <div class="list-event-thumb">
					<a href="<?=site_url('halaman/'.$halaman.'/'.$row->id)?>">
					    <img src="http://placehold.it/170x150" alt="">
					</a>
				    </div>
				    <div class="list-event-header">
					<span class="event-place small-text"><i class="fa fa-user"></i><?=$row->nama?></span>
					<span class="event-date small-text"><i class="fa fa-calendar-o"></i><?=$row->last_update?></span>
					<div class="view-details"><a href="<?=site_url('halaman/'.$halaman.'/'.$row->id)?>" class="lightBtn">Lihat lengkap</a></div>
				    </div>
				    <h5 class="event-title"><a href="<?=site_url('halaman/'.$halaman.'/'.$row->id)?>"><?=$row->title?></a></h5>
				    <?=$this->initlib->character_limiter(strip_tags($row->content), 500,'');?>
				    
				</div> <!-- /.box-content-inner -->
			    </div> <!-- /.list-event-item -->
                        <?php endif;?>
                        <?php endforeach;?>
                        
                    </div> <!-- /.col-md-12 -->

                </div> <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="load-more-btn">
                            <a href="#">Lanjutkan</a>
                        </div>
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->

            </div> <!-- /.col-md-8 -->

            <!-- Here begin Sidebar -->
            <div class="col-md-4">
		
		<?php if($halaman != 'berita'):?>
		<!-- show berita -->
		<div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Berita</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($berita->num_rows()) foreach($berita->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="blog-single.html"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='<?=site_url('halaman/berita')?>'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!-- ./show berita -->
		<?php endif;?>
		
		<?php if($halaman != 'pengumuman'):?>
		<!--show pengumuman-->
                <div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Pengumuman</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($pengumuman->num_rows()) foreach($pengumuman->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="blog-single.html"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='<?=site_url('halaman/pengumuman')?>'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!--./show pengumuman-->
		<?php endif;?>
		
		<?php if($halaman != 'peraturan'):?>
		<!-- show peraturan perundangan-->
                <div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Peraturan Perundangan</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($peraturan->num_rows()) foreach($peraturan->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="blog-single.html"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='<?=site_url('halaman/peraturan')?>'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!-- ./show peraturan perundangan-->
		<?php endif;?>
		
		<?php if($halaman != 'publikasi'):?>
		<!-- show publikasi -->
		<div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Publikasi</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($publikasi->num_rows()) foreach($publikasi->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="blog-single.html"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='<?=site_url('halaman/publikasi')?>'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!-- ./show publikasi -->
		<?php endif;?>
            </div> <!-- /.col-md-4 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container --> 
