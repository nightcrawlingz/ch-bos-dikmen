<!-- header.head -->
        <header class="head">
          <div class="search-bar">
            <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
              <i class="fa fa-expand"></i>
            </a>
            
          </div>

          <!-- ."main-bar -->
          <div class="main-bar">
            <h3>
              <i class="fa fa-dashboard"></i><?=$title?></h3>
          </div><!-- /.main-bar -->
        </header>

        <!-- end header.head -->