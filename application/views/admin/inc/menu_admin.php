<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer"> 

		<!-- start nav-right -->
		<div id="nav-right">
		
			<div class="nav-divider">&nbsp;</div>
			<div class="showhide-account"><img src="<?=$this->config->item('admin_img')?>/shared/nav/nav_myaccount.gif" width="93" height="14" alt="" /></div>
			<div class="nav-divider">&nbsp;</div>
			<a class="confirmLink-logout" href="<?=site_url('administrator/logout')?>" id="logout"><img src="<?=$this->config->item('admin_img')?>/shared/nav/nav_logout.gif" width="64" height="14" alt="" /></a>
			<div class="clear">&nbsp;</div>
		
			<!--  start account-content -->	
			<div class="account-content">
			<div class="account-drop-inner">
				<a href="<?=site_url('administrator/user')?>" id="acc-details">User Info </a>
				<div class="clear">&nbsp;</div>
				<div class="acc-line">&nbsp;</div>
				<a href="<?=site_url('administrator/password')?>" id="acc-settings">Ganti Password </a>
				
			</div>
			</div>
			<!--  end account-content -->
		
		</div>
		<!-- end nav-right -->


		<!--  start nav -->
		<div class="nav">
		<div class="table">
		
		
		                    
		<ul <?=($this->uri->segment(2)=='' || $this->uri->segment(2)=='sms') ? 'class="current"' : 'class="select"'?>>
		  <li><a href="<?=site_url('administrator')?>"><b>Pengaduan</b><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<div class="select_sub show">
			<ul class="sub">
				<li <?=($this->uri->segment(2)=='' ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator')?>">Online</a></li>
				
			</ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
		</ul>
		
		<div class="nav-divider">&nbsp;</div>
		<ul <?=$this->uri->segment(2)=='user_handling' ? 'class="current"' : 'class="select"'?> >
			<li>
				<a href="<?=site_url('administrator/user_handling')?>"><b>User Handling</b><!--[if IE 7]><!--></a><!--<![endif]-->
				<!--[if lte IE 6]><table><tr><td><![endif]-->
				<div class="select_sub <?=$this->uri->segment(2)=='user_handling' ? 'show' : ''?>">
					<ul class="sub">
						<li <?=(($this->uri->segment(3)!='edit' && $this->uri->segment(3)!='tambah') ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/user_handling')?>">List User </a></li>
						<li <?=($this->uri->segment(3)=='tambah' ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/user_handling/tambah')?>">Tambah User</a></li>
						<?php if($this->uri->segment(2)=='berita' && $this->uri->segment(3)=='edit'):?>
						<li class="sub_show"><a href="#">Edit Berita</a></li>
						<?php endif;?>
					  </ul>
				</div>
				
				<!--[if lte IE 6]></td></tr></table></a><![endif]-->
			</li>
		</ul>
		<div class="nav-divider">&nbsp;</div>
		<ul <?=$this->uri->segment(2)=='berita' ? 'class="current"' : 'class="select"'?> >
		  <li><a href="<?=site_url('administrator/berita')?>"><b>Berita</b><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<div class="select_sub <?=$this->uri->segment(2)=='berita' ? 'show' : ''?>">
			<ul class="sub">
				<li <?=(($this->uri->segment(3)!='edit' && $this->uri->segment(3)!='tambah') ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/berita')?>">List Berita </a></li>
				<li <?=($this->uri->segment(3)=='tambah' ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/berita/tambah')?>">Tambah Berita</a></li>
				<?php if($this->uri->segment(2)=='berita' && $this->uri->segment(3)=='edit'):?>
				<li class="sub_show"><a href="#">Edit Berita</a></li>
				<?php endif;?>
			  </ul>
		</div>
		
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
		</ul>
		<div class="nav-divider">&nbsp;</div>
		<ul <?=$this->uri->segment(2)=='artikel' ? 'class="current"' : 'class="select"'?> >
		  <li><a href="<?=site_url('administrator/artikel')?>"><b>Artikel</b><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<div class="select_sub <?=$this->uri->segment(2)=='artikel' ? 'show' : ''?>">
			<ul class="sub">
				<li <?=(($this->uri->segment(3)!='edit' && $this->uri->segment(3)!='tambah') ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/artikel')?>">List Artikel </a></li>
				<li <?=($this->uri->segment(3)=='tambah' ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/artikel/tambah')?>">Tambah Artikel </a></li>
				<?php if($this->uri->segment(2)=='artikel' && $this->uri->segment(3)=='edit'):?>
				<li class="sub_show"><a href="#">Edit Artikel </a></li>
				<?php endif;?>
			  </ul>
		</div>
		
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
		</ul>
		<div class="nav-divider">&nbsp;</div>
		<ul <?=$this->uri->segment(2)=='gallery' ? 'class="current"' : 'class="select"'?> >
		  <li><a href="<?=site_url('administrator/gallery')?>"><b>Gallery</b><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<div class="select_sub <?=$this->uri->segment(2)=='gallery' ? 'show' : ''?>">
			<ul class="sub">
				<li <?=(($this->uri->segment(3)!='edit' && $this->uri->segment(3)!='tambah') ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/gallery')?>">List Gallery </a></li>
				<li <?=($this->uri->segment(3)=='tambah' ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/gallery/tambah')?>">Tambah Gallery </a></li>
				<?php if($this->uri->segment(2)=='gallery' && $this->uri->segment(3)=='edit'):?>
				<li class="sub_show"><a href="#">Edit Gallery </a></li>
				<?php endif;?>
			  </ul>
		</div>
		
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
		<div class="nav-divider">&nbsp;</div>
		</ul>
		<ul <?=$this->uri->segment(2)=='about' ? 'class="current"' : 'class="select"'?> >
		  <li><a href="<?=site_url('administrator/about')?>"><b>Tentang</b><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<div class="select_sub <?=$this->uri->segment(2)=='about' ? 'show' : ''?>">
			<ul class="sub">
				<li <?=(($this->uri->segment(3)!='edit' && $this->uri->segment(3)!='view') ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/about')?>">Lihat </a></li>
				<li <?=($this->uri->segment(3)=='tambah' ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/about/tambah')?>">Edit </a></li>
			  </ul>
		</div>
		
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
		</ul>
		<div class="nav-divider">&nbsp;</div>
		<ul <?=($this->uri->segment(2)=='faq' ? 'class="current"' : 'class="select"')?>>
		  <li><a href="<?=site_url('administrator/faq')?>"><b>FAQ</b><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<div class="select_sub <?=$this->uri->segment(2)=='faq' ? 'show' : ''?>">
			  <ul class="sub">
				<li <?=($this->uri->segment(3)=='' ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/faq')?>" >Lihat FAQ</a></li>
				<li <?=($this->uri->segment(3)=='edit' ? 'class="sub_show"' : '')?>><a href="<?=site_url('administrator/faq/edit')?>" >Edit FAQ</a></li>
			  </ul>
		</div>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
		</ul>
		<div class="clear"></div>
		</div>
		<div class="clear"></div>
		</div>
		<div class="clear"></div>
		</div>
		<!--  start nav -->

</div>