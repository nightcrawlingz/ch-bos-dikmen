<?php
class Download extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('download');
    }
    function hex_str($hex){
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }
    function index($url){
        $path=$this->hex_str($url);
        $data = file_get_contents(rawurldecode($path)); // Read the file's contents
	$name = basename(rawurldecode($path));
	force_download($name, $data); 
    }
    function view($url){
        $host=$this->config->item('host');
        $data['pathfile']=$url;
        $data['namafile']=basename(rawurldecode($this->hex_str($url)));
        $data['file_url']=rawurlencode($host.$this->hex_str($data['pathfile']));
        
        $this->load->view('home/download_view',$data);
        //$this->load->view('home_view',$data);
    }
}
?>