<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SMS extends CI_Controller {
	function __construct(){
		parent::__construct();
		
		$this->load->helper(array('url','date'));
		$this->load->library(array('initlib','domparser'));
		
		$this->load->database();
		
		//$this->initlib->allow_ip(array('127.0.0.1'),$_SERVER['REMOTE_ADDR']);
		$this->load->model(array('Insert_db','Select_db','Update_db'));
		/*
		1 Telkomsel 
		2 Indosat
		3 XL
		4 Tri
		5 Axis
		6 Telkom
		7 B-tel
		8 Smart/Fren
		9 STI
		*/
	}
	function check_sms1($sms){
		$keyword=$this->Select_db->keyword();
		foreach($keyword->result() as $value){
			$arr_key[$value->keyword]=array(
			'parsing' => $value->parsing,
			'format' => $value->format,
			'variable' => $value->variable,
			'contoh' => $value->contoh,
			'sukses' => $value->sukses);
		}
		krsort($arr_key);
		$out['found_key']=0;
		$out['found_format']=0;
		foreach($arr_key as $key=>$value){
		  if(strcmp(strtolower($key.' '),strtolower(substr($sms,0,strlen($key.' '))))==0){
				$out['found_key']=1;
				extract($value);
				//$sms_format=(explode(strtolower($key).' ', strtolower($sms)));
				$sms_format=substr($sms,strlen($key.' '));
				//print_r($sms_format);
				if($sms_format!=''){
					//cek format menggunakan delimiter atau tidak
					if($parsing==''){
						//proses tanpa delimiter
						$out['found_format']=1;
						$out[$key]=array($format => $sms_format);
						//print_r($out);
					}else{
						//proses dgn delimiter
						if(count(explode($parsing,$format))==count(explode($parsing,$sms_format))){ //cek parsing sesuai atau tidak
							$out['found_format']=1;
							$format_arr=explode($parsing,$variable);
							$sms_format_arr=explode($parsing,$sms_format);
							//print_r($format_arr);
							for($i=0; $i<count($format_arr); $i++){
								$value_parsing[$format_arr[$i]]=$sms_format_arr[$i];
							}
							$out['keyword']=$key;
							$out['result']=$value_parsing;
							$out['reply']=$value['sukses'];
						}else{
							$out['found_format']=0;
							$out['reply']="format sms salah, yang benar\n".$key.($value['format']!='' ? '(spasi)' : '').$value['format']."\ncontoh: ".$key." ".$value['contoh'];
						}
					}
				}else{
					$out['found_format']=0;
					$out['reply']="format sms tidak lengkap, yang benar\n".$key.($value['format']!='' ? '(spasi)' : '').$value['format']."\ncontoh: ".$key." ".$value['contoh'];
				}
				
				break;
		  }
		}
		if($out['found_key']==0){
		  $notfound="keyword tidak ditemukan, yang tersedia\n";
		  $no=1;
		  foreach($arr_key as $key=>$value){
				$notfound.=$no.". ".$key.($value['format']!='' ? '(spasi)' : '').$value['format']."\n";
				$no++;
		  }
		  $out['reply']=$notfound;
		}
		return $out;
	}

	function check_sms($sms,$nohp,$tanggal){
		
		$keyword=$this->Select_db->keyword();
		foreach($keyword->result() as $value){
			$arr_key[$value->keyword]=array(
			'parsing' => $value->parsing,
			'format' => $value->format,
			'variable' => $value->variable,
			'contoh' => $value->contoh,
			'sukses' => $value->sukses);
		}
		//print_r($arr_key);
		krsort($arr_key);
		$out['found_key']=0;
		$out['found_format']=0;
		foreach($arr_key as $key=>$value){
		  if(strcmp(strtolower($key.' '),strtolower(substr($sms,0,strlen($key.' '))))==0){
				$out['found_key']=1;
				extract($value);
				//$sms_format=(explode(strtolower($key).' ', strtolower($sms)));
				$sms_format=substr($sms,strlen($key.' '));
				//print_r($sms_format);
				if($sms_format!=''){
					//cek format menggunakan delimiter atau tidak
					if($parsing==''){
						//proses tanpa delimiter
						$out['found_format']=1;
						$out[$key]=array($format => $sms_format);
						//print_r($out);
					}else{
						//proses dgn delimiter
						
						if(count(explode($parsing,$format))==count(explode($parsing,$sms_format))){ //cek parsing sesuai atau tidak
							$out['found_format']=1;
							$format_arr=explode($parsing,$variable);
							$sms_format_arr=explode($parsing,$sms_format);
							//print_r($format_arr);
							for($i=0; $i<count($format_arr); $i++){
								$value_parsing[$format_arr[$i]]=$sms_format_arr[$i];
							}
							//print_r($format_arr);
							$out['keyword']=$key;
							$out['result']=$value_parsing;
							$out['reply']=$value['sukses'];
						}else{
							$out['found_format']=0;
							$out['reply']="format sms salah, yang benar\n".$key.($value['format']!='' ? '(spasi)' : '').$value['format']."\ncontoh: ".$key." ".$value['contoh'];
						}
					}
				}else{
					$out['found_format']=0;
					$out['reply']="format sms tidak lengkap, yang benar\n".$key.($value['format']!='' ? '(spasi)' : '').$value['format']."\ncontoh: ".$key." ".$value['contoh'];
				}
				
				break;
		  }else{
			//echo 'update';
			$sms_last=$this->Select_db->sms_mo('last_msisdn_key_bos',array('msisdn' => $nohp))->row();
			$selisih=strtotime($tanggal) - strtotime($sms_last->date);
			//echo 'selisih = '.$selisih;
			if($selisih<=5){
				$out['found_key']=1;
				$out['found_format']=1;
				$out['keyword']=$sms_last->keyword;
				$out['sms_update']=$sms;
				$out['reply']=$value['sukses'];
				$out['id_update']=$sms_last->id;
				//echo $sms_last_result->sms.$sms;
				//$this->check_sms1($sms_last->sms.$sms,$nohp,$tanggal);
				//break;
			
			}
		  }
		}
		if($out['found_key']==0){
		  $notfound="keyword tidak ditemukan, yang tersedia\n";
		  $no=1;
		  foreach($arr_key as $key=>$value){
				$notfound.=$no.". ".$key.($value['format']!='' ? '(spasi)' : '').$value['format']."\n";
				$no++;
		  }
		  $out['reply']=$notfound;
		}
		return $out;
	}
	function index(){
		
		/*
		$date='110925174530';
		$year=substr($date,0,2);
		$month=substr($date,2,2);
		$day=substr($date,4,2);
		$hour=substr($date,6,2);
		$minute=substr($date,8,2);
		$second=substr($date,10,2);
		$data['date']=date_to_mysqldatetime($day.'-'.$month.'-'.$year.' '.$hour.':'.$minute.':'.$second.'');
		print_r($data['date']);
		*/
		
		
		$sms='bos ';
		//$arr=array('nama sekolah' => 'testing');
		//extract($arr);
		//echo $arr['nama sekolah'];
		//echo substr($sms,strlen('bos'.' '));
		//$check_sms=$this->check_sms($sms);
		//print_r($check_sms);
		//extract($check_sms['result']);
		//echo $PESAN.'-'.$LOKASI.'-'.$SEKOLAH;
		/*
		$sms='bos ';
		if($this->check_keyword($sms)=='true'){
			echo 'bener'.$sms;
		}else
			echo 'salah'.$this->check_keyword($sms);
		
		
		list($keyword,$prov,$kabkota,$sekolah,$pesan) = explode("#", $sms);
		echo strtolower($keyword).'<br>';
		echo $prov.'<br>';
		echo $kabkota.'<br>';
		echo $sekolah.'<br>';
		echo $pesan.'<br>';
		/*
		$this->output->set_output('
		<form method="post" action="http://bos.kemdikbud.go.id/pengaduan/sms/telkom_dr" mimetype="text/xml" enctype="text/xml">
		<textarea name="test"></textarea>
		<button type"submit">
		</form>						  
		');
		*/
		/*
		$data=$this->Select_db->sms_mo('get_hash');
		$msisdn=array();
		foreach($data->result() as $row){
			$hash[]=$row->hash;
		}
		
		$hash=array(substr(md5('1278'),0,4),substr(md5('34910'),0,4),substr(md5('5611'),0,4));
		$md5='1278';
		do{
			$md5=md5($md5);
			echo 'check '.$md5.'<br>';
		}while(in_array(substr($md5,0,4),$hash));
		echo $md5.'<br>';
		print_r($hash);
		echo 'lolos';
		*/
	}
	/*
	function message($no){
		$pesan=array(
				'sukses' => 'Terima kasih, pengaduan anda telah masuk ke sistem kami'
				);
		return $pesan[$no];
	}
	*/
	function ip_error(){
		$this->output->set_output(
		header("Content-Type:text/xml").'
		<SMS>
			<CODE>-2</CODE>
			<STATUS>restricted access, ip address not allowed</STATUS>
		</SMS>'
		);
	}
	
	function sti(){
		//setting STI
		/*
		Keterangan value parameter sbb:
		- <MSISDN> = nomor tujuan format international tanpa tanda '+' (contoh: 6282812345678)
		- <USER> = user account KPK (akan diinformasikan kemudian)
		- <PASS> = password account KPK (akan diinformasikan kemudian)
		- <PASS_ESME> = password access code KPK (akan diinformasikan kemudian)
		- <PUSH_DATE> = timestamp reply format YYMMDDHHmmss (contoh: 110925174530)
		- <SMS_ID> = ID unik sms dari sisi KPK untuk tracking, format angka maksimal 25 digits
		- <TEXT> = Isi reply SMS ke pelanggan maksimal 160 karakter
		*/
		//pull
		$this->initlib->allow_ip(array('127.0.0.1','202.159.6.164'));
		$data['log_mo']=json_encode($this->input->get());
		$data['id_operator']=9; //STI
		$data['msisdn']=$this->input->get('msisdn');
		$date=$this->input->get('push_date');
		$year=substr($date,0,2);
		$month=substr($date,2,2);
		$day=substr($date,4,2);
		$hour=substr($date,6,2);
		$minute=substr($date,8,2);
		$second=substr($date,10,2);
		$data['date']=date_to_mysqldatetime();
		$data['sms_id']=$this->input->get('sms_id');
		$data['sms']=$this->input->get('text');
		
		$check_sms=$this->check_sms($data['sms'],$data['msisdn'],$data['date']);
		if($check_sms['found_key']==1 && $check_sms['found_format']==1)
			$data['keyword']=$check_sms['keyword'];
			
		//check param
		if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['sms']!='' && $data['log_mo']!='' && !in_array('',$this->input->get(),true)){
			if($psms['id_sms']=$this->Insert_db->sms_mo($data)){
				if($check_sms['found_key']==1 && $check_sms['found_format']==1){
					//insert ke table pol_psms
					//echo 'keyword='.$data['keyword'];
					if($data['keyword']=='BOS'){
						if(isset($check_sms['id_update'])){//update
							$row_psms=$this->Select_db->psms(array('id_sms' => $check_sms['id_update']))->row();
							$data_update=array(
								'id_sms' => $psms['id_sms'],
								'pengaduan' => $row_psms->pengaduan.$check_sms['sms_update']
							);
							$this->Update_db->psms1(array('id_sms' => $check_sms['id_update']),$data_update);
						}
						else{//insert
							$psms['msisdn']=$data['msisdn'];
							//print_r($check_sms['result']);
							extract($check_sms['result']);
							$psms['pengaduan']=$PESAN;
							$psms['sekolah']=$SEKOLAH;
							$psms['lokasi']=$LOKASI;
							$psms['time']=date_to_mysqldatetime();
							$psms['id_operator']=$data['id_operator'];
							$this->Insert_db->pengaduan_sms($psms);
						}
					}
				}
				
				/*
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<MO>
					<CODE>0</CODE>
					<STATUS>message processed successfully</STATUS>
				</MO>'
				);
				*/
				//push kirim balik
				$user='bos1771';
				$password='bos1771';
				$serviceid='bos1771'; //esme
				$mt['msisdn']=$data['msisdn'];
				$mt['id_operator']=9;//STI
				$mt['date']=date_to_mysqldatetime();
				$mt['sms']=$check_sms['reply'];
				$mt['sms_id']=$data['sms_id'];
				
				
				$param=array(
				'msisdn' => $mt['msisdn'],
				'usercp' => $user,
				'passcp' => $password,
				'esme' => $serviceid,
				'pd' => date("ymdHis"),
				'trans_id' => $mt['sms_id'],
				'txt' => urlencode($mt['sms'])
				);
				//http://202.159.6.165:5050/smsApps/smsCp/push.jsp?msisdn=<MSISDN>&usercp=<USER>&passcp=<PASS>&esme=<PASS_ESME>&pd=<PUSH_DATE>&trans_id=<SMS_ID>&txt=<TEXT>
				$url='http://202.159.6.165:5050/smsApps/smsCp/push.jsp';
				$mt['http_response'] = http_get($url.$this->initlib->param_url($param), array("timeout"=>10), $info);
				$mt['http_info']=json_encode($info);
				$mt['log_mt']=$info['effective_url'];
				
				$this->Insert_db->sms_mt($mt);
				$response='sukses';
				if($response=='sukses'){ //??
					//update reply
				}
			}else{
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<MO>
					<CODE>1</CODE>
					<STATUS>system error, failed to insert data</STATUS>
				</MO>'
				);
			}
		
		}else{
			$this->output->set_output(
			header("Content-Type:text/xml").'
			<MO>
				<CODE>-1</CODE>
				<STATUS>parameter incomplete</STATUS>
			</MO>'
			);
		}
		
		
		//end setting STI
	}
	
	function indosat(){
		//setting INDOSAT
		/*
		param:
		msisdn
			MSISDN number / Subscriber mobile number in international format (62)
		sms
		trx_time
			Message Creation time from SMSC/CDMS with following format
			YYYYMMDDhhmmss
		substype
			Subscriber type:
			• substype=10 , means matrix subscriber
			• substype=20 , means mentari or im3 subscriber
			• substype=30 , means matrix hybrid subscriber
		transid
				Transaction ID generated by CDMS
				Note: For CP that used MO Pull or MO+MT with Session billing
				model, they must use the same transid from the correlated MO
				message when submit the MT message as a reply to subscriber
			
		sc
				ShortCode(sc) sender
		*/
		$this->initlib->allow_ip(array('127.0.0.1','202.152.162.221','118.98.223.88'));
		$data['log_mo']=json_encode($this->input->get());
		$data['id_operator']=2; //indosat
		$data['msisdn']=$this->input->get('msisdn');
		$date=$this->input->get('trx_time');
		$year=substr($date,0,4);
		$month=substr($date,4,2);
		$day=substr($date,6,2);
		$hour=substr($date,8,2);
		$minute=substr($date,10,2);
		$second=substr($date,12,2);
		$data['date']=date_to_mysqldatetime();
		$data['sms_id']=$this->input->get('transid');
		$data['sms']=$this->input->get('sms');
		
		$check_sms=$this->check_sms($data['sms'],$data['msisdn'],$data['date']);
		if($check_sms['found_key']==1 && $check_sms['found_format']==1)
			$data['keyword']=$check_sms['keyword'];
		
		//buat kode reff
		/*
		$hash=$this->Select_db->sms_mo('get_hash');
		$arr_hash=array();
		foreach($hash->result() as $row){
			$arr_hash[]=$row->hash;
		}
		
		$md5=md5($data['msisdn'].$data['sms'].$data['date'].$data['id_operator']);
		do{
			$md5=md5($md5);
		}while(in_array(substr($md5,0,4),$arr_hash));
		
		$data['hash']=substr($md5,0,4);
		//$data['hash']='1234';
		*/
		//end buat kode reff
		
		
		//check param
		if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['sms']!='' && $data['log_mo']!='' && !in_array('',$this->input->get(),true)){
			if($psms['id_sms']=$this->Insert_db->sms_mo($data)){
				if($check_sms['found_key']==1 && $check_sms['found_format']==1){
					//insert ke table pol_psms
					//echo 'keyword='.$data['keyword'];
					if($data['keyword']=='BOS'){
						if(isset($check_sms['id_update'])){//update
							$row_psms=$this->Select_db->psms(array('id_sms' => $check_sms['id_update']))->row();
							$data_update=array(
								'id_sms' => $psms['id_sms'],
								'pengaduan' => $row_psms->pengaduan.$check_sms['sms_update']
							);
							$this->Update_db->psms1(array('id_sms' => $check_sms['id_update']),$data_update);
						}
						else{//insert
							$psms['msisdn']=$data['msisdn'];
							//print_r($check_sms['result']);
							extract($check_sms['result']);
							$psms['pengaduan']=$PESAN;
							$psms['sekolah']=$SEKOLAH;
							$psms['lokasi']=$LOKASI;
							$psms['time']=date_to_mysqldatetime();
							$psms['id_operator']=$data['id_operator'];
							$this->Insert_db->pengaduan_sms($psms);
						}
					}
				}
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<MO>
					<STATUS>0</STATUS>
					<TRANSID>'.$data['sms_id'].'</TRANSID>
					<STATUS>Message processed successfully</STATUS>
				</MO>'
				);
				
				//push kirim balik
				$user='bos';
				$password='bospwd';
				$serviceid='17710140001002';
				//$serviceid='17710140008001';
				$mt['msisdn']=$data['msisdn'];
				$mt['id_operator']=2;//indosat
				$mt['date']=date_to_mysqldatetime();
				$mt['sms']=$check_sms['reply'];
				$mt['sms_id']=$data['sms_id'];
				
				$param=array(
				'uid' => $user,
				'pwd' => $password,
				'serviceid' => $serviceid,
				'msisdn' => $mt['msisdn'],
				'sms' => urlencode($mt['sms']),
				'transid' => $mt['sms_id'],
				'smstype' => '0'
				);
				//http://<ip>:<port>/?uid=<..>&pwd=<..>&serviceid=<..>&msisdn=<..>&sms=<..>&transid=<..>&smstype=<..>
				$url='http://202.152.162.221:55000';
				$mt['http_response'] = http_get($url.$this->initlib->param_url($param), array("timeout"=>1), $info);
				$mt['http_info']=json_encode($info);
				$mt['log_mt']=$info['effective_url'];
				$this->Insert_db->sms_mt($mt);
				
				$html=$this->domparser->str_get_html($mt['http_response']);
				if($html->find('STATUS', 0)->plaintext == 0){
					//sukses kirim
				}
				
			}else{
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<MO>
					<STATUS>3</STATUS>
					<TRANSID>'.$data['sms_id'].'</TRANSID>
					<STATUS>System Error</STATUS>
				</MO>'
				);
			}
		
		}else{
			$this->output->set_output(
			header("Content-Type:text/xml").'
			<MO>
				<STATUS>-1</STATUS>
				<TRANSID>'.$data['sms_id'].'</TRANSID>
				<STATUS>Parameter incomplete</STATUS>
			</MO>'
			);
		}
		//end setting INDOSAT
	}
	function indosat_dr(){
		//setting DR INDOSAT
		/*
		 http://<ip>:<port>/?time=<..>&serviceid=<..>&tid=<..>&dest=<..>&status=<..>
		param:
		dest
			MSISDN number / Subscriber mobile number in international format (62)
		serviceid
			Service ID that had been provision in CDMS
		time
			Message Creation time from SMSC/CDMS with following format YYYYMMDDhhmmss. 
		tid
			Transaction ID from the correlated MT message 
		status
			MT message status. For detail information about status 
			parameter please refer to Status description on Delivery 
			Message
		*/
		$this->initlib->allow_ip(array('127.0.0.1','202.152.162.221','118.98.223.88'));
		$data['log_dr']=json_encode($this->input->get());
		$data['id_operator']=2; //indosat
		$data['msisdn']=$this->input->get('dest');
		$date=$this->input->get('time');
		$year=substr($date,0,4);
		$month=substr($date,4,2);
		$day=substr($date,6,2);
		$hour=substr($date,8,2);
		$minute=substr($date,10,2);
		$second=substr($date,12,2);
		$data['date']=date_to_mysqldatetime();
		$data['sms_id']=$this->input->get('tid');
		$status=$this->input->get('status');
		$msg_status=array(
			2 => 'Success',
			3 => '
				MT message failed. This error can be 
				happen for following cases: 
				• SMS Undelivered 
				• Indosat System failure. E.g. Subscriber 
				type checking failure 
				• Internal CDMS failure',
			4 => '
				Insufficient balance
				Subscriber number in grace period. ',
			5 => 'Charging failed',
			6 => '
				- Subscriber number not found 
				- Subscriber not registered to SPM service ',
			7 => 'Invalid Service ID',
			8 => '
				Invalid Transaction ID (for MO pull and MO+MT with session only)',
			97 => 'Subscription Hit Limit Exceeded (only for SPM Service) ',
			99 => 'CP Throttle'
						  );
		//check param
		if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['log_dr']!='' && !in_array('',$this->input->get(),true)){
			if($this->Insert_db->sms_dr($data)){
				
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<DR>
					<STATUS>'.$status.'</STATUS>
					<TRANSID>'.$data['sms_id'].'</TRANSID>
					<MSG>'.$msg_status[$status].'</MSG>
				</DR>'
				);
				
				
			}else{
				/*
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<MO>
					<STATUS>3</STATUS>
					<TRANSID>'.$data['sms_id'].'</TRANSID>
					<STATUS>System Error</STATUS>
				</MO>'
				);
				*/
			}
		
		}else{
			/*
			$this->output->set_output(
			header("Content-Type:text/xml").'
			<MO>
				<STATUS>-1</STATUS>
				<TRANSID>'.$data['sms_id'].'</TRANSID>
				<STATUS>Parameter incomplete</STATUS>
			</MO>'
			);
			*/
		}
		//end setting DR INDOSAT
	}
	function telkomsel(){
		//setting TELKOMSEL
		/*
		param:
		msisdn
			MSISDN of the sender. It includes the country code (CC) and the network destination code (NDC) e.g.62811917676.
		sms
			Message content sent from subscriber
		trx_id
			Unique number generated by Telkomsel.
		adn
			Destination shortcode
		Example of request to CP URL:
		MO http://118.98.223.88:80/pengaduan/sms/telkomsel?msisdn=<msisdn_sender>&trx_id=<number>&sms=<sms_content>&adn=<adn>

		*/
		$this->initlib->allow_ip(array('127.0.0.1','202.3.208.6','118.98.223.88'));
		$data['log_mo']=json_encode($this->input->get());
		$data['id_operator']=1; //telkomsel
		$data['msisdn']=$this->input->get('msisdn');
		$data['date']=date_to_mysqldatetime();
		$data['sms_id']=$this->input->get('trx_id');
		$data['sms']=$this->input->get('sms');
		
		$check_sms=$this->check_sms($data['sms'],$data['msisdn'],$data['date']);
		if($check_sms['found_key']==1 && $check_sms['found_format']==1)
			$data['keyword']=$check_sms['keyword'];
		
		//check param
		if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['sms']!='' && $data['log_mo']!='' && !in_array('',$this->input->get(),true)){
			if($psms['id_sms']=$this->Insert_db->sms_mo($data)){
				if($check_sms['found_key']==1 && $check_sms['found_format']==1){
					//insert ke table pol_psms
					//echo 'keyword='.$data['keyword'];
					if($data['keyword']=='BOS'){
						if(isset($check_sms['id_update'])){//update
							$row_psms=$this->Select_db->psms(array('id_sms' => $check_sms['id_update']))->row();
							$data_update=array(
								'id_sms' => $psms['id_sms'],
								'pengaduan' => $row_psms->pengaduan.$check_sms['sms_update']
							);
							$this->Update_db->psms1(array('id_sms' => $check_sms['id_update']),$data_update);
						}
						else{//insert
							$psms['msisdn']=$data['msisdn'];
							//print_r($check_sms['result']);
							extract($check_sms['result']);
							$psms['pengaduan']=$PESAN;
							$psms['sekolah']=$SEKOLAH;
							$psms['lokasi']=$LOKASI;
							$psms['time']=date_to_mysqldatetime();
							$psms['id_operator']=$data['id_operator'];
							$this->Insert_db->pengaduan_sms($psms);
						}
					}
				}
				/*
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<MO>
					<STATUS>0</STATUS>
					<TRANSID>'.$data['sms_id'].'</TRANSID>
					<STATUS>Message processed successfully</STATUS>
				</MO>'
				);
				*/
				
				//push kirim balik
				/*
				appsid
					Defined by Telkomsel.
				pwd
					Defined by Telkomsel.
				sid
					Defined by Telkomsel.
				msisdn
					MSISDN of the sender that will be charged by Telkomsel.
					It includes the country code (CC) and the network
					destination code (NDC) e.g.62811917676.
				recipient (optional)
					MSISDN of the recipient (optional). If this variable is not
					set, content will be delivered to msisdn variable above.
				sms
					Reply message content sent to subscriber. Maximum message length defined by Telkomsel
				sender (optional)
					Additional suffix that will be added at the end of
					shortcode. Whether or this suffix can be added for a
					particular shortcode is defined by Telkomsel. (optional)
				trx_id
					Unique number generated by Telkomsel
				*/
				$user='bos';
				$password='bos273';
				$serviceid='MDS_Y_C04000_0000_PULL';
				$mt['msisdn']=$data['msisdn'];
				$mt['id_operator']=1;//telkomsel
				$mt['date']=date_to_mysqldatetime();
				$mt['sms']=$check_sms['reply'];
				$mt['sms_id']=$data['sms_id'];
				$url='http://202.3.208.6:8100/cp/submitSM.jsp';
				//$url='http://www.bos.site/pengaduan/sms/testing_mt';
				$param=array(
				'appsid' => $user,
				'pwd' => $password,
				'sid' => $serviceid,
				'msisdn' => $mt['msisdn'],
				'sms' => urlencode($mt['sms']),
				'trx_id' => $mt['sms_id'],
				);
				$mt['http_response'] = http_get($url.$this->initlib->param_url($param), array("timeout"=>10), $info);
				$mt['http_info']=json_encode($info);
				$mt['log_mt']=$info['effective_url'];
				$this->Insert_db->sms_mt($mt);
				
				$html=$this->domparser->str_get_html($mt['http_response']);
				if($html->find('STATUS', 0)->plaintext == 0){
					//sukses kirim
				}
				
			}else{
				/*
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<MO>
					<STATUS>3</STATUS>
					<TRANSID>'.$data['sms_id'].'</TRANSID>
					<STATUS>System Error</STATUS>
				</MO>'
				);
				*/
			}
		
		}else{
			/*
			$this->output->set_output(
			header("Content-Type:text/xml").'
			<MO>
				<STATUS>-1</STATUS>
				<TRANSID>'.$data['sms_id'].'</TRANSID>
				<STATUS>Parameter incomplete</STATUS>
			</MO>'
			);
			*/
		}
		//end setting TELKOMSEL
	}
	function tri(){
		//setting TRI
		/*
		HTTP GET Method
		MO
		-- 
		http://<Server-IP>:<Server-Port>?msisdn=<MSISDN>&shortcode=<ShortCode>&message=<Message>
		MT
		--
		http://<Server-ip>:<Server-port>/PUSH?USERNAME=<username>&PASSWORD=<password>&REG_DELIVERY=<reg_delivery>&ORIGIN_ADDR=<shortcode>&MOBILENO=<msisdn>&TYPE=<msg_type>
		
		Parameters:
		6.	<server-ip> 		: IP of Server
		7.	<server-port> 		: Port of Server
		8.	<username>		: User name of CP
		9.	<password>		: Password of CP
		10.	<reg_delivery>		: integer value only (1 for DRL Required, 0 for no DLR Required)
		11.	<shortcode>		: Shortcode
		12.	<msisdn>		: Mobile Number
		13.	<msg_type>		: This filed show the type of message. This having 3 values (0 for Text Msg, 1 for Binary Msg & 2 for Unicode msg)
		
		DR
		--
		
		http://<Server-ip>:<Server-port>/PUSH?msisdn=<MSISDN>&shortcode=<ShortCode>&status=<DR Status>&messageid=<submission ID>
		*/
		//$this->initlib->allow_ip(array('127.0.0.1','202.152.162.221','118.98.223.88'));
		$data['log_mo']=json_encode($this->input->get());
		$data['id_operator']=4; //tri
		$data['msisdn']=$this->input->get('mobile_no');
		$data['date']=date_to_mysqldatetime();
		$data['sms_id']='tri# '.time();
		$data['sms']=$this->input->get('message');
		
		$check_sms=$this->check_sms($data['sms'],$data['msisdn'],$data['date']);
		if($check_sms['found_key']==1 && $check_sms['found_format']==1)
			$data['keyword']=$check_sms['keyword'];
			
		//check param
		if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['sms']!='' && $data['log_mo']!='' && !in_array('',$this->input->get(),true)){
			if($psms['id_sms']=$this->Insert_db->sms_mo($data)){
				if($check_sms['found_key']==1 && $check_sms['found_format']==1){
					//insert ke table pol_psms
					//echo 'keyword='.$data['keyword'];
					if($data['keyword']=='BOS'){
						if(isset($check_sms['id_update'])){//update
							$row_psms=$this->Select_db->psms(array('id_sms' => $check_sms['id_update']))->row();
							$data_update=array(
								'id_sms' => $psms['id_sms'],
								'pengaduan' => $row_psms->pengaduan.$check_sms['sms_update']
							);
							$this->Update_db->psms1(array('id_sms' => $check_sms['id_update']),$data_update);
						}
						else{//insert
							$psms['msisdn']=$data['msisdn'];
							//print_r($check_sms['result']);
							extract($check_sms['result']);
							$psms['pengaduan']=$PESAN;
							$psms['sekolah']=$SEKOLAH;
							$psms['lokasi']=$LOKASI;
							$psms['time']=date_to_mysqldatetime();
							$psms['id_operator']=$data['id_operator'];
							$this->Insert_db->pengaduan_sms($psms);
						}
					}
				}
				
				//jika insert sukses
				
				//push kirim balik
				$user='kdm1771mt0';
				$password='password';
				//$serviceid='';
				$mt['msisdn']=$data['msisdn'];
				$mt['id_operator']=4;//tri
				$mt['date']=date_to_mysqldatetime();
				$mt['sms']=$check_sms['reply'];
				$mt['sms_id']=$data['sms_id'];
				
				$param=array(
				//'REQUESTTYPE' => 'SMSSubmitReq',
				'USERNAME' => $user,
				'PASSWORD' => $password,
				'REG_DELIVERY' => '0', //- 1   Request SMSC Receipt 
				'MOBILENO' => $mt['msisdn'],
				'ORIGIN_ADDR' => '17710', //no bos 1771
				'MESSAGE' => urlencode($mt['sms'])
				//'TYPE' => '1' //0 for text message
				);
				//http://<Server-ip>:<Server-port>/PUSH?USERNAME=<username>&PASSWORD=<password>&REG_DELIVERY=<reg_delivery>&ORIGIN_ADDR=<shortcode>&MOBILENO=<msisdn>&TYPE=<msg_type>
				$url='http://180.214.234.52:8080/PUSH'; 
				$mt['http_response'] = http_get($url.$this->initlib->param_url($param), array("timeout"=>10), $info);
				$mt['http_info']=json_encode($info);
				$mt['log_mt']=$info['effective_url'];
				$this->Insert_db->sms_mt($mt);
				
				/*
				$html=$this->domparser->str_get_html($mt['http_response']);
				if($html->find('STATUS', 0)->plaintext == 0){
					//sukses kirim
				}
				*/
				
			}else{
				//jika insert gagal
			}
		
		}else{
			//jika param tdk lengkap
			
		}
		//end setting TRI
	}
	function tri_dr(){
		//setting DR TRI
		/*
		http://<Server-ip>:<Server-port>/PUSH?msisdn=<MSISDN>&shortcode=<ShortCode>&status=<DR Status>&messageid=<submission ID>
		*/
		//$this->initlib->allow_ip(array('127.0.0.1','202.152.162.221'));
		$data['log_dr']=json_encode($this->input->get());
		$data['id_operator']=4; //tri
		$data['msisdn']=$this->input->get('msisdn');
		$data['date']=date_to_mysqldatetime();
		$data['sms_id']=$this->input->get('messageid');
		
		
		//check param
		if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['log_dr']!='' && !in_array('',$this->input->get(),true)){
			if($this->Insert_db->sms_dr($data)){
				//jika insert sukses
			}else{
				//jika insert gagal
			}
		
		}else{
			//jika param tdk lengkap
			
		}
		//end setting DR TRI
	}
	
	function telkom(){
		
		//setting telkom flexi
		/*
		XML MO (1)
		<?xml version='1.0' ?>
		<message id='routerSMSRouter1@cmsapp1:3465977'> <------ SMS-ID generated by CMS
		<sms type='mo'> <------ sms type mo
			<destination><address><number type='abbreviated'>1234</number></address></destination>
			<source><address><number type='national'>0217070XXXX</number></address></source>
		<ud type='text'>REG DOA</ud> <------ body of SMS is HTML Entities Encode
		</sms>
		</message>
		
		XML MT (2)
		<?xml version='1.0'?>
		<message>
		<sms type="'mt"> <------ sms type mt
		<destination messageid=“1234#DOA#1234567890"> <------ SMS-ID must generated by CP
		<address>
		<number type="national">0217070XXXX</number>
		</address>
		</destination>
		<source><address><number type='abbreviated'>1234</number></address></source>
		<ud type="text">Reply Message</ud> <------ body of SMS must HTML Entities Encode
		<rsr type="success_failure"/> <------ DR Report flag request
		</sms>
		</message>
		
		XML DR (3)
		<?xml version='1.0' ?>
		<message id='routerSMSRouter1@cmsapp1:4685979'>
		<sms type='dr'> <------ sms type dr
		<destination messageid=‘1234#DOA#1234567890'> <------ SMS-ID correlate with SMS-ID in MT
		<address><number type='national'>0217070XXXX</number></address></destination>
		<source><address><number type='abbreviated'>1234</number></address></source>
		<rsr type='success'/> <------ DR status
		</sms>
		</message>
		*/
		//$this->initlib->allow_ip(array('127.0.0.1','202.152.162.221','118.98.223.88'));
		//$_GET['in'];
		
	
		$in=urldecode($this->input->get('xml')); //http raw post data
		$xml=$this->domparser->str_get_html($in);
		$type=$xml->find("sms", 0)->type;
 
		if($type=='mo'){
			//get mo
			$data['log_mo']=trim($in);
			$data['id_operator']=6; //telkom
			$data['msisdn']=$xml->find('sms[type=mo]',0)->find('source',0)->find('address',0)->find('number',0)->innertext;
			$data['date']=date_to_mysqldatetime();
			$data['sms_id']=$xml->find('message',0)->id;
			$data['sms']=$xml->find('sms[type=mo]',0)->find('ud',0)->innertext;
			
			$check_sms=$this->check_sms($data['sms'],$data['msisdn'],$data['date']);
			if($check_sms['found_key']==1 && $check_sms['found_format']==1)
				$data['keyword']=$check_sms['keyword'];
			
			//check param
			if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['sms']!='' && $data['log_mo']!='' ){
				if($psms['id_sms']=$this->Insert_db->sms_mo($data)){
						
					if($check_sms['found_key']==1 && $check_sms['found_format']==1){
						//insert ke table pol_psms
						//echo 'keyword='.$data['keyword'];
						if($data['keyword']=='BOS'){
							if(isset($check_sms['id_update'])){//update
								$row_psms=$this->Select_db->psms(array('id_sms' => $check_sms['id_update']))->row();
								$data_update=array(
									'id_sms' => $psms['id_sms'],
									'pengaduan' => $row_psms->pengaduan.$check_sms['sms_update']
								);
								$this->Update_db->psms1(array('id_sms' => $check_sms['id_update']),$data_update);
							}
							else{//insert
								$psms['msisdn']=$data['msisdn'];
								//print_r($check_sms['result']);
								extract($check_sms['result']);
								$psms['pengaduan']=$PESAN;
								$psms['sekolah']=$SEKOLAH;
								$psms['lokasi']=$LOKASI;
								$psms['time']=date_to_mysqldatetime();
								$psms['id_operator']=$data['id_operator'];
								$this->Insert_db->pengaduan_sms($psms);
							}
						}
					}
					//push kirim balik
					$user='';
					$password='';
					//$serviceid='';
					//$serviceid='17710140008001';
					$mt['msisdn']=$data['msisdn'];
					$mt['id_operator']=6;//telkom
					$mt['date']=date_to_mysqldatetime();
					$mt['sms']=$check_sms['reply'];
					$mt['sms_id']='flexi#'.time();
					
$mt['log_mt']="<?xml version='1.0'?>
<message>
	<sms type='mt'>
		<destination messageid='".$mt['sms_id']."'>
			<address>
				<number type='national'>".$mt['msisdn']."</number>
			</address>
		</destination>
		<source><address><number type='abbreviated'>1771</number></address></source>
		<ud type='text'>".htmlentities($mt['sms'])."</ud>
	</sms>
</message>
";
					
					//$url='http://222.124.198.148:9972/httpadapter';
					
					
					$this->output->set_output($mt['log_mt']);
					$this->Insert_db->sms_mt($mt);
					
				}else{
					//gagal insert
				}
			
			}else{
				//parameter tdk lengkap
			}
			//end setting telkomm  flexi
			//end get mo
		}elseif($type=='dr'){
			//get dr
			$this->output->set_output('OK');
		
			//$in=urldecode(file_get_contents("php://input")); //http raw post data
			//$xml=$this->domparser->str_get_html($in);
			
			$data['log_dr']=$in;
			$data['id_operator']=6; //telkom flexi
			$data['msisdn']=$xml->find('sms[type=dr]',0)->find('address',0)->find('number',0)->innertext;
			$data['date']=date_to_mysqldatetime();
			$data['sms_id']=$xml->find('message',0)->id;
			
			
			//check param
			if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['log_dr']!='' ){
				if($this->Insert_db->sms_dr($data)){
					//jika insert sukses
				}else{
					//jika insert gagal
				}
			
			}else{
				//jika param tdk lengkap
				
			}
			//end get dr
		}
		
	}
	function esia(){
		//setting ESIA
		/*
		MO:
		http://192.168.1.205/smsmo/httppull.php?dn=622196834021&to=1234&msg=test&id=0924831212&date=20090106_163407
		dn : no pengirim
		to : no tujuan/short number 1771
		msg : pesan sms
		id : id pesan
		date : YYYYMMDD_HHMMSS
		*/

		//$this->initlib->allow_ip(array('127.0.0.1','202.3.208.6','118.98.223.88'));
		$data['log_mo']=json_encode($this->input->get());
		$data['id_operator']=7; //b-tel/esia
		$data['msisdn']=$this->input->get('dn');
		$data['date']=date_to_mysqldatetime();
		$data['sms_id']=$this->input->get('id');
		$data['sms']=$this->input->get('msg');
		
		$check_sms=$this->check_sms($data['sms'],$data['msisdn'],$data['date']);
		if($check_sms['found_key']==1 && $check_sms['found_format']==1)
			$data['keyword']=$check_sms['keyword'];
		
		//check param
		if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['sms']!='' && $data['log_mo']!='' && !in_array('',$this->input->get(),true)){
			if($psms['id_sms']=$this->Insert_db->sms_mo($data)){
				if($check_sms['found_key']==1 && $check_sms['found_format']==1){
					//insert ke table pol_psms
					//echo 'keyword='.$data['keyword'];
					if($data['keyword']=='BOS'){
						if(isset($check_sms['id_update'])){//update
							$row_psms=$this->Select_db->psms(array('id_sms' => $check_sms['id_update']))->row();
							$data_update=array(
								'id_sms' => $psms['id_sms'],
								'pengaduan' => $row_psms->pengaduan.$check_sms['sms_update']
							);
							$this->Update_db->psms1(array('id_sms' => $check_sms['id_update']),$data_update);
						}
						else{//insert
							$psms['msisdn']=$data['msisdn'];
							//print_r($check_sms['result']);
							extract($check_sms['result']);
							$psms['pengaduan']=$PESAN;
							$psms['sekolah']=$SEKOLAH;
							$psms['lokasi']=$LOKASI;
							$psms['time']=date_to_mysqldatetime();
							$psms['id_operator']=$data['id_operator'];
							$this->Insert_db->pengaduan_sms($psms);
						}
					}
				}
				
				/*
				MT
				
				*/
				$user='';
				$password='';
				$serviceid='';
				$mt['msisdn']=$data['msisdn'];
				$mt['id_operator']=7;//b-tel/esia
				$mt['date']=date_to_mysqldatetime();
				$mt['sms']=$check_sms['reply'];
				$mt['sms_id']=$data['sms_id'];
				$url='http://202.152.195.23:8050/sms/pushsms';
				
				$param=array(
				'usr' => $user,
				'pwd' => $password,
				'mtcode' => $serviceid,
				'dn' => $mt['msisdn'],
				'from' => '1771',
				'msg' => urlencode($mt['sms']),
				'moid' => $mt['sms_id'],
				);
				$mt['http_response'] = http_get($url.$this->initlib->param_url($param), array("timeout"=>10), $info);
				$mt['http_info']=json_encode($info);
				$mt['log_mt']=$info['effective_url'];
				$this->Insert_db->sms_mt($mt);
				
				$html=$this->domparser->str_get_html($mt['http_response']);
				if($html->find('STATUS', 0)->plaintext == 0){
					//sukses kirim
				}
				
			}else{
				/*
				$this->output->set_output(
				header("Content-Type:text/xml").'
				<MO>
					<STATUS>3</STATUS>
					<TRANSID>'.$data['sms_id'].'</TRANSID>
					<STATUS>System Error</STATUS>
				</MO>'
				);
				*/
			}
		
		}else{
			/*
			$this->output->set_output(
			header("Content-Type:text/xml").'
			<MO>
				<STATUS>-1</STATUS>
				<TRANSID>'.$data['sms_id'].'</TRANSID>
				<STATUS>Parameter incomplete</STATUS>
			</MO>'
			);
			*/
		}
		//end setting TELKOMSEL
	}
	/*
	function telkom_dr(){
		
		//setting DR telkom flexi
		
		

		
		//$this->initlib->allow_ip(array('127.0.0.1','202.152.162.221'));
		$this->output->set_output('OK');
		
		$in=urldecode(file_get_contents("php://input")); //http raw post data
		$xml=$this->domparser->str_get_html($in);
		
		$data['log_dr']=$in;
		$data['id_operator']=6; //telkom flexi
		$data['msisdn']=$xml->find('sms[type=dr]',0)->find('address',0)->find('number',0)->innertext;
		$data['date']=date_to_mysqldatetime();
		$data['sms_id']=$xml->find('message',0)->id;
		
		
		//check param
		if($data['msisdn']!='' && $data['date']!='' && $data['sms_id']!='' && $data['log_dr']!='' ){
			if($this->Insert_db->sms_dr($data)){
				//jika insert sukses
			}else{
				//jika insert gagal
			}
		
		}else{
			//jika param tdk lengkap
			
		}
		//end setting DR telkom flexi
		
	}
	*/
	
	function testing(){
		/*
		$this->db_sms = $this->load->database('sms', TRUE);
		$data=array(
				'KdPengaduan' => '1234'
		);
		$this->db_sms->insert('temp_pol_psms',$data);
		$id = $this->db_sms->insert_id();
		echo $id;
		*/
		if($id=16)
			echo "true".$id;
		else
			echo "false";
	}
	function ShortURL($integer, $chr='abcdefghijklmnopqrstuvwxyz0123456789') {    
	// the $chr has all the characters you want to use in the url's;    
		$base = strlen($chr);
	// number of characters = base
		$string = '';
		do {
			// start looping through the integer and getting the remainders using the base
			$remainder = $integer % $base;      
			// replace that remainder with the corresponding the $chr using the index
			$string .= $chr[$remainder];
			// reduce the integer with the remainder and divide the sum with the base
			$integer = ($integer - $remainder) / $base;
		} while($integer > 0);
	
		   // continue doing that until integer reaches 0;
		return $string;
	
	}
	function LongURL($string, $chr='abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') {
       // this is just reversing everything that was done in the other function, one important thing to note is to use the same $chr as you did in the ShortURL
        $array = array_flip(str_split($chr));

        $base = strlen($chr);
        $integer = 0;
        $length = strlen($string);


        for($c = 0; $c < $length; ++$c) {
            $integer += $array[$string[$c]] * pow($base, $length - $c - 1);
        }
        return $integer;


    }
	function testing_mt(){
		print_r($_GET);
	}
	
}